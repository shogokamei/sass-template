# Sass-template

静的コーディングの際の、品質保全のためのテンプレートです

このリポジトリで実現できること

- docker を使用したローカル環境に依存しない構築
- ソースコードを自動整形
- 静的解析ツールによる SCSS のデバッグ、自動修正
- SCSS の自動コンパイル

## 前提条件

- Visual Studio Code のインストールが事前に完了している事
  [![Visual Studio Code Install](https://img.shields.io/badge/Visual%20Studio%20Code-Install-blue.svg)](https://code.visualstudio.com/Download)

- Docker Desktop のインストールが事前に完了している事
  [![Docker Desktop Install](https://img.shields.io/badge/Docker%20Desktop-Install-blue.svg)](https://docs.docker.com/desktop/install/windows-install/)

- Git のインストールが事前に完了している事
  [![Git Install](https://img.shields.io/badge/Git-Install-blue.svg)](https://git-scm.com/download/win)

## 拡張機能

このテンプレートで使用する Visual Studio Code の拡張機能

[![Live Sass Compiler Docs](https://img.shields.io/badge/Live%20Sass%20Compiler-Docs-green.svg)](https://marketplace.visualstudio.com/items?itemName=ritwickdey.live-sass)
[![Prettier Docs](https://img.shields.io/badge/Prettier-Docs-green.svg)](https://prettier.io/)
[![Dev Containers](https://img.shields.io/badge/Dev%20Containers-Docs-green.svg)](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)
[![Stylelint Docs](https://img.shields.io/badge/Stylelint-Docs-green.svg)](https://stylelint.io/)

## セットアップ

| 実行場所 | 説明                           |
| -------- | ------------------------------ |
| [Win]    | Windows cmd 上にて実行         |
| [Docker] | Docker コンテナ内 cmd にて実行 |

### 1. Git リポジトリをクローン

プロジェクトを作成したい階層にカレントディレクトリを移動して以下を実行

```bash
  [Win] git clone https://gitlab.com/shogokamei/sass-template.git
```

### 2. Docker の構築＆実行

1.でクローンしたディレクトリ内（`docker-compose.yml` ファイルが存在する階層）にカレントディレクトリを移動して以下を実行

```bash
  [Win] docker-compose up -d --build
```

Docker Desktop でコンテナのアイコンが緑に、STATUS が Running なっていれば OK

![Watch Sass](https://trans-it.net/img/docker-running.png)

一度、build でサーバを構築したら次回から、Docker Desktop の Start ボタン、または以下のコマンドのみで OK

![Watch Sass](https://trans-it.net/img/docker-start.png)

```bash
  [Win] docker-compose up -d
```

### 3. Visual Studio Code をコンテナにアタッチ

拡張機能 `Dev Containers` をインストール

- Dev Containers
  [![Dev Containers](https://img.shields.io/badge/Dev%20Containers-Docs-blue.svg)](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)

サイドバーにリモートエクスプローラーが表示されます。
その中の開発コンテナ > 任意のコンテナで`→`アイコンをクリックすると、Visual Studio Code をコンテナにアタッチできます（画像参照）

![Watch Sass](https://trans-it.net/img/attach.png)

エクスプローラー > フォルダを開く から以下ディレクトを開く

```
  /var/www/html/
```

**注意:**
改行コードの関係で git に差分がでる可能性あり。その際は Git の設定ファイル（`C:\Users\[user]\.gitconfig`）を見直す。以下のように、`autocrlf`を `input`（または `false`）に変更する。設定の記載がない場合で改行コードの差分が出る場合は、Git の GUI クライアントの設定が邪魔をしている可能性があるため追記する

```
[core]
	autocrlf = input
```

### 4. Node.js のパッケージをインストール

```bash
  [Docker] npm install
```

node_modules ディレクトリが生成されます

### 5. コンテナ内に拡張機能をインストール

以下の拡張機能をコンテナ内にインストールします

- Live Sass Compiler
  [![Live Sass Compiler Docs](https://img.shields.io/badge/Live%20Sass%20Compiler-Docs-blue.svg)](https://marketplace.visualstudio.com/items?itemName=ritwickdey.live-sass)

- Prettier
  [![Prettier Docs](https://img.shields.io/badge/Prettier-Docs-blue.svg)](https://prettier.io/)

- Stylelint
  [![Stylelint Docs](https://img.shields.io/badge/Stylelint-Docs-blue.svg)](https://stylelint.io/)

## 動作確認

### 1.Prettier、Stylelint の動作確認

`project/assets/scss/home.scss`を開き、以下のように h1 の`font-size`と`color`を入れ替える

```scss
h1 {
  color: $color-main;
  font-size: rem(12);
}
```

無駄なインデントや改行を入れて保存をした際に、スタイルの並び順や改行等が自動的に修正されれば OK

### 2.Live Sass Compiler の動作確認

下部バーにある![Watch Sass](https://trans-it.net/img/watch_sass.png)アイコンから Live Sass Compiler を起動。アイコンが見当たらない場合は、Visual Studio Code を再起動すると表示される。または`Shift + Ctrl + P` から以下コマンドで Live Sass Compiler を起動することもできます

![Watch Sass](https://trans-it.net/img/watch_sass-cmd.png)

```
  watch Sass
```

ターミナルの出力が以下のようになれば scss ファイルの変更を監視し自動的にコンパイルしてくれます

```
--------------------
Watching...
--------------------
```

任意の scss ファイルを開き、保存し直した際に`project/assets/css`内に以下ファイルが自動生成されれば OK

- ●●●●.css // 表示確認用の css
- ●●●●.css.map //.css と.scss を繋ぎ合わせる設定ファイル
- ●●●●.min.css // minify 処理後の css 実際読み込むファイル
- ●●●●.min.css.map //.css と.scss を繋ぎ合わせる設定ファイル

## Scss 記載ルール

CSS 設計手法`FLOCSS（フロックス）`を参考に簡略化してルール化しています。レイヤーの概念はかなりはしょっているため、巨大プロジェクトの際は公式ドキュメントを参照ください

[![FLOCSS Docs](https://img.shields.io/badge/FLOCSS-Docs-green.svg)](https://github.com/hiloki/flocss)

### ディレクトリ構成

```
.
├── .vscode
│   └── settings.json
├── project
    ├── assets
        ├── css　// コンパイル後のcssを格納
        ├── img
        ├── js
        └── scss
            ├── base
                ├── _common.scss // 全ベージの土台となる共通スタイル
                ├── _reset.scss // destyle.min.cssを読み込み
                └── destyle.min.css // リセットCSS
            ├── lyout // ヘッダーフッター等の共通パーツのスタイル
            ├── utils
                ├── _function.scss // ファンクション定義ファイル
                ├── _mixin.scss // mixin定義ファイル
                └── _variable.scss // 変数定義ファイル
            ├── home.scss // 個ページ専用スタイル
            ├── style.scss // 全ページに必要なものをまとめたファイル
    └── index.html
    └── info.php
├── .gitignore
・
・
・
```

### 命名規則

ID、クラス名の先頭には、レイヤーや状態などの`頭文字`と`-`で区切り、その後にブロック名を記載

| レイヤー・状態 | ID、クラス | 説明                           |
| -------------- | ---------- | ------------------------------ |
| base           | b-block    | 全ベージの土台となる共通なもの |
| lyout          | l-block    | ヘッダ、フッタ等の共通パーツ   |
| page           | p-block    | そのページのみで使用           |
| js 連動        | js-open    | javascript で制御する要素      |
| 状態           | is-active  | ボタンの状態など               |

エレメント以下は`__`と`-`で区切る

| レイヤー・状態 | ID、クラス               |
| -------------- | ------------------------ |
| Element        | p-block\_\_element       |
| Element 以下   | p-block\_\_element-class |

### コメントアウト

大見出し

```scss
/* ===============================
	大見出し
================================ */
```

小見出し

```scss
/*	小見出し
================================*/
```
